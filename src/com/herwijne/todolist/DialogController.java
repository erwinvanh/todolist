package com.herwijne.todolist;

import com.herwijne.todolist.datamodel.TodoData;
import com.herwijne.todolist.datamodel.TodoItem;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.time.LocalDate;

public class DialogController {

    @FXML
    private TextField shortDescriptionField;
    @FXML
    private TextArea detailArea;
    @FXML
    private DatePicker deadlinePicker;

    private static BooleanBinding inputsFull ;

    public static BooleanBinding inputsFullBinding() {
        return inputsFull ;
    }

    public final boolean getInputsFull() {
        return inputsFull.get();
    }
    public void initialize() {
        inputsFull = new BooleanBinding() {
            {
                bind(shortDescriptionField.textProperty(),
                        detailArea.textProperty());
            }
            @Override
            protected boolean computeValue() {
                return ! (shortDescriptionField.getText().trim().isEmpty()
                        || detailArea.getText().trim().isEmpty());
            }
        };
        deadlinePicker.setValue(LocalDate.now());
    }

    public void initData(TodoItem editItem){
        shortDescriptionField.setText(editItem.getShortDescription());
        detailArea.setText(editItem.getDetails());
        deadlinePicker.setValue(editItem.getDeadLine());
    }

    public TodoItem procesResults(){
        String shortDescription = shortDescriptionField.getText().trim();
        String details = detailArea.getText().trim();
        LocalDate deadlineValue = deadlinePicker.getValue();

        TodoItem newItem = new TodoItem(shortDescription, details, deadlineValue);
        TodoData.getInstance().addTodoItem(newItem);
        return newItem;
    }

    public TodoItem procesResults(TodoItem currentItem){
        String shortDescription = shortDescriptionField.getText().trim();
        String details = detailArea.getText().trim();
        LocalDate deadlineValue = deadlinePicker.getValue();

        TodoItem newItem = new TodoItem(shortDescription, details, deadlineValue);
        TodoData.getInstance().updateTodoItem(currentItem, newItem);
        return newItem;
    }
}
