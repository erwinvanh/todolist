package com.herwijne.todolist;

import com.herwijne.todolist.datamodel.TodoItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import java.time.LocalDate;

public class TodoItemListCellFactory implements Callback<ListView<TodoItem>, ListCell<TodoItem>> {


    private ContextMenu listContextMenu;

    public TodoItemListCellFactory(ContextMenu listContextMenu) {
        this.listContextMenu = listContextMenu;
    }

    @Override
    public ListCell<TodoItem> call(ListView<TodoItem> param) {
        return new ListCell<TodoItem>() {

            @Override
            protected void updateItem(TodoItem item, boolean empty) {
                super.updateItem(item, empty);

                getStyleClass().removeAll( "green-cell", "red-cell");

                if (empty || item == null) {
                    setText(null);
                    setContextMenu(null);
                } else {
                    setText(item.getShortDescription());
                    setContextMenu(listContextMenu);

                    if (item.getDeadLine().isBefore(LocalDate.now().plusDays(1))) {
                        getStyleClass().add("red-cell");
                    } else if (item.getDeadLine().equals(LocalDate.now().plusDays(1))) {
                        getStyleClass().add("green-cell");
                    }
                }
            }
        };
    }

}
