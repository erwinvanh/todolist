package com.herwijne.todolist;

import com.herwijne.todolist.datamodel.TodoData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("mainwindow.fxml"));
        primaryStage.setTitle("ToDo List");
        Scene scene = new Scene(root, 900,500);    // create scene
        String styleSheet = Main.class.getResource("css/main.css").toExternalForm();    // load css file
        scene.getStylesheets().add(styleSheet);    // add stylesheet
        primaryStage.setScene(scene);    // set scene to stage
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        try {
            TodoData.getInstance().storeTodoItems();


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void init() throws Exception {
        try {
            TodoData.getInstance().loadTodoItems();


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
