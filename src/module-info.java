module TodoList {
    requires javafx.fxml;
    requires javafx.controls;
    requires gson;
    requires java.sql;

    opens com.herwijne.todolist;
}